﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Homer.Service.HomerApi.Application;

namespace Homer.Service.HomerApi.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureHomerService(this IServiceCollection services)
        {
            return services.AddMediatR(typeof(ApplicationAnchor));
        }
    }
}
