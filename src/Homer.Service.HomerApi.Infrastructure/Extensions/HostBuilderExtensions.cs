﻿using Homer.Service.HomerApi.Application;
using Lamar;
using Lamar.Microsoft.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Homer.Service.HomerApi.Infrastructure.Extensions
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder ConfigureContainerDefaults(this IHostBuilder hostBuilder)
        {
            return hostBuilder.UseLamar()
                .ConfigureContainer<ServiceRegistry>((_, services) =>
                {
                    services.Scan(scanner =>
                    {
                        scanner.TheCallingAssembly();
                        scanner.AssemblyContainingType<ApplicationAnchor>();
                        scanner.AssemblyContainingType<InfrastructureAnchor>();
                        scanner.WithDefaultConventions();
                    });
                });
        }
    }
}
