﻿using System.Threading;
using System.Threading.Tasks;
using Homer.Service.HomerApi.Application.UseCases.Queries.ViewModels;
using MediatR;

namespace Homer.Service.HomerApi.Application.UseCases.Queries
{
    public class GetValue
    {
        public record Query() : IRequest<MyViewModel> { };

        public class Handler : IRequestHandler<Query, MyViewModel>
        {
            public Task<MyViewModel> Handle(Query request, CancellationToken cancellationToken)
            {
                var response = new MyViewModel("My value");
                return Task.FromResult(response);
            }
        }
    }
}
