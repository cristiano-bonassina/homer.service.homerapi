namespace Homer.Service.HomerApi.Application.UseCases.Queries.ViewModels
{
    public record MyViewModel(string Value) { }
}
