using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Homer.Service.HomerApi.Infrastructure.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Homer.Service.HomerApi
{
    public class Program
    {
        public static async Task Main()
        {
               var host = new HostBuilder()
                .ConfigureFunctionsWorkerDefaults()
                .ConfigureContainerDefaults()
                .ConfigureServices(services =>
                {
                    services.Configure<JsonSerializerOptions>(options =>
                    {
                        options.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault;
                        options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    });
                    services.ConfigureHomerService();
                })
                .Build();
            await host.RunAsync();
        }
    }
}