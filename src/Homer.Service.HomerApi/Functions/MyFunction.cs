using System.Net;
using System.Threading.Tasks;
using Homer.Service.HomerApi.Application.UseCases.Queries;
using MediatR;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Homer.Service.HomerApi.Functions
{
    public static class MyFunction
    {
        [Function(nameof(MyFunction))]
        public static async Task<HttpResponseData> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "v1/get-value")]
            HttpRequestData req,
            FunctionContext context
            )
        {
            var mediator = context.InstanceServices.GetRequiredService<ISender>();
            var request = new GetValue.Query();
            var response = await mediator.Send(request);
            var httpResponseData = req.CreateResponse(HttpStatusCode.OK);
            await httpResponseData.WriteAsJsonAsync(response);

            return httpResponseData;
        }
    }
}
